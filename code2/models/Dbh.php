<?php

class Dbh {
	private $host = "db:3306";
	private $user = "root";
	private $pwd = "rootpassword";
	private $dbName = "testdb";

	protected function connect()
	{
		$dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbName;
		$pdo = new PDO($dsn, $this->user, $this->pwd);
		$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
		// $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		return $pdo;
	}
}
