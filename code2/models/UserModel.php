<?php

class UserModel extends Model
{
	public $id;
	public $uid;
	public $email;
	private $login;
	private $password;

	public function getAll()
	{
		$sql = "SELECT id, uid, email FROM users";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute();
		$result = $stmt->fetchAll();
		return $result;
	}

	public function getUser($uid)
	{
		$sql = "SELECT * FROM users WHERE uid = :uid OR email = :uid";
		$stmt = $this->connect()->prepare($sql);
		// vd(["PARAMS" => $uid]);
		$stmt->execute(['uid' => $uid]);
		$result = $stmt->fetch();
		// $this->uid = $result->uid;
		// $this->email = $result->email;
		return $result;
	}

	public function setUser($uid, $email, $pwd)
	{
		if ($this->getUser($uid)) {
			$this->message = 'User already exist. Please choose another name';
			return false;
		} else {
			$sql = "INSERT INTO users(uid, email, pwd) VALUES(?, ?, ?)";

			$hashedPassword = password_hash($pwd, PASSWORD_DEFAULT);

			$stmt = $this->connect()->prepare($sql);
			$stmt->execute([$uid, $email, $hashedPassword]);
			$this->message = 'User has been created';

			return $this->getUser($uid);
		}
	}
}


/* CREATE TABLE users (
id int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
uid TINYTEXT NOT NULL,
email TINYTEXT NOT NULL,
pwd LONGTEXT NOT NULL
); */
