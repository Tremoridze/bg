<?php

class TaskModel extends Model
{
	public $id;
	private $uid;
	private $email;
	private $done;
	private $text;
	private $created_at;
	private $done_at;

	public function newTask($uid, string $email, string $text)
	{
		$done = 0;

		$created_at = new DateTime('now');
		$created_at = $created_at->format('Y-m-d H:i');
		$sql = "INSERT INTO tasks(uid, email, done, text, created_at, changed_at) VALUES(?, ?, ?, ?, ?, ?)";
		$stmt = $this->connect()->prepare($sql);

		$result = $stmt->execute([$uid, $email, $done, $text, $created_at, $created_at]);

		return $result;
	}

	public function getAllTasks()
	{
		$sql = "SELECT id, uid, email, done, text, editoruid FROM tasks";
		// $sql = "SELECT * FROM tasks";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute();

		$result = $stmt->fetchAll();
		return $result;
	}

	public function getTaskByUid($uid)
	{
		$sql = "SELECT id, uid, email, done, text, editoruid FROM tasks WHERE uid = ?";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$uid]);

		$result = $stmt->fetchAll();
		return $result;
	}

	public function getById($id)
	{
		$sql = "SELECT * FROM tasks WHERE id = ?";
		// $sql = "SELECT id, uid, email, done, text FROM tasks WHERE id = ?";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([$id]);

		$result = $stmt->fetch();
		return $result;
	}

	public function updateTask($id, $text, $done = 0, $editoruid)
	{
		$changed_at = new DateTime('now');
		$changed_at = $changed_at->format('Y-m-d H:i');

		$sql = "UPDATE tasks SET done = :done, text = :text, editoruid = :editoruid, changed_at = :changed_at WHERE id=:id";
		$stmt = $this->connect()->prepare($sql);
		$stmt->execute([
			"id" => $id,
			"text" => $text,
			"done" => $done,
			"editoruid" => $editoruid,
			"changed_at" => $changed_at
		]);

		return $stmt->fetch();
	}
}

/*
  CREATE TABLE `tasks` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `uid` tinytext NOT NULL,
  `email` tinytext NOT NULL,
  `done` tinyint NULL,
  `text` longtext NOT NULL,
  `created_at` datetime NULL,
  `changed_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `editoruid` tinytext NULL
);
 */