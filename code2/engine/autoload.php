<?php
date_default_timezone_set('Europe/Kiev');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once "engine/util.php";
require_once "engine/route.php";

spl_autoload_register('autoloaderFn');

Route::start();

function autoloaderFn($className)
{
	if (file_exists("./controllers/$className.php")) {
		include_once("./controllers/$className.php");
	}
	elseif (file_exists("./models/$className.php")) {
		include_once("./models/$className.php");
	}
	else {
		echo "Path with classname $className not exist";
	}
}
