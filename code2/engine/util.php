<?php

function vd()
{
	echo "<pre>";
	foreach (func_get_args() as $key => $value) {
		var_dump($value);
	}
	echo "</pre>";
}

function pr()
{
	echo "<pre>";
	foreach (func_get_args() as $key => $value) {
		print_r($value);
	}
	echo "</pre>";
}