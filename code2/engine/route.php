<?php

class Route {
	public $controller;
	public $action;

	static function start() {
		$params = [];
		foreach ($_GET as $key => $value) {
			if ($key == 'route') {
				$routes = explode('/', $value);
			} else {
				$params[$key] = $value;
			}
		}

		$controller = empty($routes[1]) ? "Task" : ucwords($routes[1]);
		$action = empty($routes[2]) ? "index" : $routes[2];
		if(empty($routes[1])) {
			header("Location: /task");
		}

		if(method_exists($controller, $action)) {
			$class = new $controller($params);
			$result = $class->$action($params);
		} else {
			// 404
		}
		// vd($class);
	}
}