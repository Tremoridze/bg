<?php

class User extends Controller
{
	public $params;
	public $message;

	public function __construct($params)
	{
		$this->params = $params;
		$this->userModel = new UserModel();
		$this->task = new Task($params);
	}

	public function index()
	{
		$data = $this->userModel->getAll();
		return $this->render("user_view", $data);
	}

	public function create()
	{
		if (!isset($_POST['signup-submit'])) {
			header("Location: /");
		}
		$uid = $_POST['uid'];
		$email = $_POST['email'];
		$pwd = $_POST['pwd'];

		if ($uid && $email && $pwd) {
			$result = $this->userModel->setUser($uid, $email, $pwd);
			$this->message = $this->userModel->message;
			if ($result) {
				$this->index();
			} else {
				$this->signup();
			}
		} else {
			return false;
		}
	}

	private function findOne($uid)
	{
		$user = $this->userModel->getUser($uid);
		return $user;
	}

	public function login($params)
	{
		if (!isset($_POST['login-submit'])) {
			header("Location: /");
		}
		$uid = $_POST['uid'];
		$pwd = $_POST['pwd'];
		$user = $this->findOne($uid);
		if ($user) {
			$isPwdCOrrect = password_verify($pwd, $user->pwd);
			if ($isPwdCOrrect) {
				$_SESSION['uid'] = $user->uid;
				$_SESSION['email'] = $user->email;
				$this->message = 'You have been logged in';

				header("Location: /task");
				// $this->task->index();
			}
		} else {
			$this->render("401", $uid, $params);
		}
	}

	public function logout($params)
	{
		session_destroy();
		if (ini_get('register_globals')) {
			foreach ($_SESSION as $key => $value) {
				if (isset($GLOBALS[$key]))
					unset($GLOBALS[$key]);
			}
		}
		$this->message = 'You have been logged out';
		header("Location: /");
		// $this->task->index();
		// $this->render("user/logout");
	}

	public function signup()
	{
		return $this->render("user/signup_view", null);
	}
}
