<?php

abstract class Controller extends Dbh
{
	protected function render($template, $data = [])
	{
		include("views/header_view.php");
		$path = "views/$template.php";

		if (isset($this->message)) {
			echo '<div class="alert alert-success" role="alert">' . $this->message . "</div>";
		}
		include($path);
		include("views/footer_view.php");
	}
}
