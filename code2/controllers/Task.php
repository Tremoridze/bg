<?php
require_once "models/TaskModel.php";


class Task extends Controller
{
	public $message;
	public $params;
	public $title;

	public function __construct($params)
	{
		$this->params = $params;
		$this->taskModel = new TaskModel();
	}

	public function createTask($params)
	{
		if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['text'])) {
			$uid = $_POST['name'];
			$email = $_POST['email'];
			$text = trim($_POST['text']);

			$result = $this->taskModel->newTask($uid, $email, $text);
			$message = $result == true ? "Task has been created" : "Task creating has been failed!";
			$params['message'] = $message;
			$this->message = $message;

			$this->create($params);
		}
	}

	public function index()
	{
		$result = $this->taskModel->getAllTasks();
		return $this->render("task/task_view", $result);
	}

	public function create($params)
	{
		return $this->render("task/create_view", null, $params);
	}

	public function my($params)
	{
		if (isset($_SESSION['uid'])) {
			$uid = $_SESSION['uid'];
			$this->title = "My tasks";
			$result = $this->taskModel->getTaskByUid($uid);
			$this->render("task/task_view", $result, $params);
		} else {
			$this->index($params);
		}
	}

	public function edit($params)
	{
		$id = $params['id'];
		$task = $this->taskModel->getById($id);
		return $this->render("task/edit_view", $task, $params);
	}

	public function editTask()
	{
		if (isset($_SESSION['uid']) && $_SESSION['uid'] == 'admin' ) {
			$id = $_POST['id'];
			$text = trim($_POST['text']);
			$done = isset($_POST['done']) ? 1 : 0;
			$editoruid = $_SESSION['uid'];

			$this->taskModel->updateTask($id, $text, $done, $editoruid);
			$this->open(["id" => $id]);
		} else {
			$params['message'] = "You need to be logged in as administrator";
			$this->index($params);
		}
	}

	public function open($params)
	{
		$id = $params['id'];
		$task = $this->taskModel->getById($id);
		return $this->render("task/open_view", $task, $params);
	}
}
