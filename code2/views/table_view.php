	<div class="container">
		<table class="table table-responsive-md table-fixed col-md-12 layout-fixed">
			<tr>
				<?php
				if (isset($data[0])) {
					$page = !empty($_GET['page']) ? (int) $_GET['page'] : 1;
					$limit = !empty($_GET['limit']) ? (int) $_GET['limit'] : 3;
					$params = $this->params;

					$dataKeys = array_keys(get_object_vars($data[0]));
					$totalRows = sizeof($data);
					$totalPages = ceil($totalRows / $limit);
					$page = max($page, 1); //get 1 page when $_GET['page'] <= 0
					$page = min($page, $totalPages); //get last page when $_GET['page'] > $totalPages
					$offset = ($page - 1) * $limit;
					if ($offset < 0) $offset = 0;

					$canSort = (isset($params['sortby']) && in_array($params['sortby'], $dataKeys));
					$direction = isset($params['direction']) ? $params['direction']  : 'ASC';

					if ($canSort && $direction == 'DESC') {
						$nextDirection = 'ASC';
					} else {
						$nextDirection = 'DESC';
					}

					if ($canSort && $direction == "ASC") {
						array_multisort(array_column($data, $params['sortby']), SORT_ASC, $data);
					} elseif ($canSort) {
						array_multisort(array_column($data, $params['sortby']), SORT_DESC, $data);
					}

					$data = array_slice($data, $offset, $limit);

					include("table_generator.php");

					if ($totalPages > 1) {
						include("pagination_view.php");
					}
				} else {
					echo "<hr>No data in $controller";
				}
				?>
		</table>
		<?php
		if (isset($params['sortby'])) {
			echo 'Sorted by ' . $params['sortby'] . " " . $direction . '</h6>';
		}
		?>
	</div>