<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../public/css/styles.css">
	<link rel="stylesheet" type="text/css" href="../public/fontawesome/css/all.css">
	<link rel="stylesheet" href="../public/css/bootstrap.min.css">
	<title>Beegee Tasker</title>
</head>

<body>
	<header>
	</header>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark sticky-top">
		<a href="/" class="navbar-brand">
			<img src="../public/img/beejee_small.png" class="logo">
		</a>
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link" href="/">Home</a></li>
			<li class="nav-item"><a class="nav-link" href="/task">Tasks</a></li>
			<?php
			if (isset($_SESSION['email'])) {
				echo '<li class="nav-item"><a class="nav-link" href="/task/my">My Tasks</a></li>';
			}
			if (isset($_SESSION['uid']) && isset($_SESSION['email']) && $_SESSION['uid'] == 'admin') {
				echo '<li class="nav-item"><a class="nav-link" href="/user">Users</a></li>';
			}
			?>
			<!-- <li class="nav-item"><a class="nav-link" href="#">Contacts</a></li> -->
		</ul>
		<?php
		if (isset($_SESSION['email'])) {
			echo '<li class="nav-item nav-link ml-auto">';
			echo "<div class='nav-link text-info'>{$_SESSION['email']}</div>";
			echo '
						</li>
						<form class="form-inline" action="/user/logout" method="POST">
							<button class="btn btn-success" type="submit" name="logout-submit">Logout</button>
						</form>
						';
		} else {
			include_once "views/user/login_form.php";
		} ?>
	</nav>
	<div class="container">