<thead class='thead-light'>

<?php
foreach (get_object_vars($data[0]) as $key => $value) {
	$thisSorted = $canSort && $key == $params['sortby'];
	$query = $_GET;
	$query['route'] = isset($query['route']) ? $query['route'] : $controller;
	$query['sortby'] = $key;
	$query['direction'] = ($thisSorted ? $nextDirection : $direction);
	$queryBuild = http_build_query($query, null, '&');
	$href = str_replace("route=%2F", "/", $queryBuild);
	echo "<th class='th_$key " . ($thisSorted ? "active" : '') . " '><a href='$href'>$key</a></th>";
}

if ($controller == "task") {
	echo '<th class="th_button">Open</th>';
}
if ($controller == "task" && isset($_SESSION['uid']) && isset($_SESSION['email']) && $_SESSION['uid'] == 'admin') {
	echo '<th class="th_button">Edit</th>';
	// echo '<th class="th_button">Delete</th>';
}
?>
</thead>

<tbody>
<?php

foreach ($data as $key => $values) {
	echo "<tr>";
	foreach ($values as $row => $value) {
		echo "<td class='tr_{$row} $row'>" . htmlspecialchars($value) . "</td>";
	}
	if ($controller == "task") {

		echo "<td class='tr_button'><a href='/{$controller}/open?id={$values->id}'><i class='fas fa-search'></i></a></td>";
	}
	if ($controller == "task" && isset($_SESSION['uid']) && isset($_SESSION['email']) && $_SESSION['uid'] == 'admin') {
		echo "<td class='tr_button'><a href='/{$controller}/edit?id={$values->id}'><i class='far fa-edit'></i></a></td>";
		// echo "<td class='tr_button'><a class='text-danger' href='/{$controller}/delete?id={$values->id}'><i class='fas fa-trash'></i></a></td>";
	}
	echo "</tr>";
}
?>
</tbody>