<div class="col-md-offset-3">
	<h1 class="h1">Signup</h1>
	<form class="form form-signin col-md-6" action="/user/create" method="post">
		<input class="form-control" type="text" name="uid" id="uid" placeholder="Username" required>
		<input class="form-control" type="email" name="email" id="email" placeholder="E-mail" required>
		<input class="form-control" type="password" name="pwd" id="pwd" placeholder="Password" required>
		<!-- <input class="form-control" type="password" name="pwd-repeat" id="pwd-repeat" placeholder="Repeat Password" required> -->
		<button class="btn btn-success" type="submit" name="signup-submit">Signup</button>
	</form>
</div>