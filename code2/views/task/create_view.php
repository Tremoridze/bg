<h1 class="h1">Create a new task</h1>
<form class="col-sm-8 form-input" action="/task/createTask" method="post">
	<label for="task">Task text</label>
	<input class="form-control" type="name" name="name" id="name" placeholder="Enter your name" required value="<?php echo (isset($_SESSION['uid']) ? $_SESSION['uid'] : ''); ?>">
	<input class="form-control" type="email" name="email" id="email" placeholder="Enter your email" required value="<?php echo (isset($_SESSION['email']) ? $_SESSION['email'] : ''); ?>">
	<textarea class="form-control" type="text" name="text" id="taskText" required placeholder="Enter task text please"></textarea>
	<input class="btn btn-success" type="submit" id="submit-task" value="Create">
</form>
<?php
