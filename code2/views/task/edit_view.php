<h1 class="h1">Edit task</h1>
<form class="col-md-12 form-input" action="/task/editTask" method="post">
	<div class="form-group row">
		<label for="taskId" class="col-sm-2 col-form-label">Task Id</label>
		<div class="col-sm-8">
			<input class="form-control-plaintext" type="id" name="id" id="taskId" required readonly value="<?php echo $data->id; ?>">
		</div>
	</div>

	<div class="form-group row">
		<label for="taskUid" class="col-sm-2 col-form-label">Task user Name</label>
		<div class="col-sm-8">
			<input class="form-control-plaintext" type="name" name="name" id="taskUid" required readonly value="<?php echo $data->uid; ?>">
		</div>
	</div>

	<div class="form-group row">
		<label for="taskName" class="col-sm-2 col-form-label">Task email</label>
		<div class="col-sm-8">
			<input class="form-control-plaintext" type="email" name="email" id="taskName" required readonly value="<?php echo $data->email; ?>">
		</div>
	</div>

	<div class="form-group row">
		<label class="col-sm-2" for="taskText">Task text</label>
		<textarea class="form-control col-sm-8" type="text" name="text" id="taskText" required value=""><?php echo $data->text; ?></textarea>
	</div>

	<div class="form-group row">
		<div class="form-check col-sm-2">
			<input class="form-check-input" type="checkbox" value="Finished" id="taskdone" name="done" <?php echo ($data->done == true ? 'checked=""' : '') ?>>
			<label class="form-check-label col-form-label-md" for="taskdone">Done</label>
		</div>

		<input class="btn btn-outline-success col-sm-8" type="submit" id="submit-task" value="Edit">
	</div>
</form>
<?php
