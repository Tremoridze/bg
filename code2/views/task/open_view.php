<h1 class="h1">View task #<?php echo $data->id; ?></h1>
<form class="col-md-12 form-input" action="/task/edit?id=<?php echo $data->id; ?>" method="post">
	<div class="form-group row">
		<label for="taskUid" class="col-sm-3 col-form-label">Task user Name</label>
		<div class="col-sm-8">
			<input class="form-control-plaintext" type="name" name="name" id="taskUid" readonly value="<?php echo $data->uid; ?>">
		</div>
	</div>

	<div class="form-group row">
		<label for="taskName" class="col-sm-3 col-form-label">Task email</label>
		<div class="col-sm-8">
			<input class="form-control-plaintext" type="email" name="email" id="taskName" readonly value="<?php echo $data->email; ?>">
		</div>
	</div>
	<div class="form-group row">
		<label for="taskCreated" class="col-sm-3 col-form-label">Created at</label>
		<div class="col-sm-8">
			<input class="form-control-plaintext col-sm-8" type="text" name="created_at" id="taskCreated" readonly value="<?php echo $data->created_at; ?>">
		</div>
	</div>

	<div class="form-group row">
		<label for="taskChanged" class="col-sm-3 col-form-label">Changed at</label>
		<div class="col-sm-8">
			<input class="form-control-plaintext col-sm-8" type="text" name="changet_at" id="taskChanged" readonly value="<?php echo $data->changed_at; ?>">
		</div>
	</div>

	<div class="form-group row">
		<label for="taskEdited" class="col-sm-3 col-form-label">Edited by</label>
		<div class="col-sm-8">
		<input class="form-control-plaintext col-sm-8" type="text" name="editoruid" id="taskEdited" readonly value="<?php echo $data->editoruid; ?>">
		</div>
	</div>

	<div class="form-group row">
		<label for="taskText" class="col-sm-3 col-form-label">Task text</label>
		<div class="col-sm-8">
			<textarea class="form-control-plaintext col-sm-8" type="text" name="text" id="taskText" readonly><?php echo htmlspecialchars($data->text); ?></textarea>
		</div>
	</div>


	<div class="form-group row">
		<div class="form-check col-sm-3">
			<input class="form-check-input" type="checkbox" value="Finished" id="taskdone" name="done" <?php echo ($data->done == 1? 'checked=""' : '') ?> disabled>
			<label class="form-check-label col-form-label-md" for="taskdone">Done</label>
		</div>

		<?php
		if (isset($_SESSION['uid']) && isset($_SESSION['email']) && $_SESSION['uid'] == 'admin') {
			echo '<input class="btn btn-success col-sm-8" type="submit" id="submit-task" value="Edit">';
		} ?>
	</div>
</form>
<?php
