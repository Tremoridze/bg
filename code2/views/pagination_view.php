<nav aria-label="Page navigation mx-left">
	<ul class="pagination">
		<?php
		$query = $_GET;
		for ($i = 1; $i <= $totalPages; $i++) {
			$active = $i == $page ? "active" : '';
			$query['route'] = isset($query['route']) ? $query['route'] : $controller;

			$query['page'] = $i;
			$queryBuild = http_build_query($query, null, '&');

			$href = str_replace("route=%2F", "/",  $queryBuild);
			echo "<li class='page-item $active'><a class='page-link' href='$href'>$i</a></li>";
		}
		?>
	</ul>
</nav>